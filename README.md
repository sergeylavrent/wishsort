# Wishsort
Create hierarchies of multiple items through simplified comparisons.
You can sort a large list of strings by comparing two at a time.
This project is simple and is just an introduction to C and its concepts for me.

## Compile & Run for Linux
```sh
git clone https://gitlab.com/sergeylavrent/wishsort
cd wishsort
make
```
